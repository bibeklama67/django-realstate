from django.db import models
from realtors.models import Realtor
# Create your models here.
class Listing(models.Model):
	merchant=models.ForeignKey(Realtor,on_delete=models.DO_NOTHING)
	title=models.CharField(max_length=255)
	address=models.CharField(max_length=255)
	price=models.IntegerField(default='00')
	description=models.TextField(blank=True)
	sqft=models.DecimalField(max_digits=10,decimal_places=2)
	compounds=models.BooleanField(default=True)
	rooms=models.IntegerField()
	floor=models.IntegerField()
	road=models.DecimalField(max_digits=10,decimal_places=2)
	bathroom=models.IntegerField()
	hall=models.BooleanField(default=True)
	kitchen=models.IntegerField()
	garage=models.BooleanField(default=True)
	photo_1=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_2=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_3=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_4=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_5=models.ImageField(upload_to="%y/%m/%d",blank=True)

	def __str__(self):
		return self.title
