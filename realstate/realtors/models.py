from django.db import models
from datetime import datetime
# Create your models here.
class Realtor(models.Model):
	name=models.CharField(max_length=255)
	photo = models.ImageField(upload_to="%y/%m/%d",default='')
	phone = models.CharField(max_length=255)
	address=models.CharField(max_length=255)
	description = models.TextField(blank=True)
	gender = models.CharField(max_length=255)
	affiliations = models.CharField(max_length=255)
	is_active  = models.BooleanField(default=True)
	reg_date=models.DateTimeField(default=datetime.now)

	def __str__(self):
		return self.name