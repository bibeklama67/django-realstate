from django.shortcuts import render
from realtors.models import Realtor
from listings.models import Listing
# Create your views here.
def index(request):
	listings = Listing.objects.order_by("id")
	realtors = Realtor.objects.all()
	context = {
		'listings' : listings,
		'realtors' : realtors,
	}
	return render(request,'pages/index.html',context)

def search(request):
	queryset_list = Listing.objects.order_by("id")
	#keywords
	if 'keywords' in request.GET:
		keywords = request.GET['keywords']
		if keywords:
			queryset_list = queryset_list.filter(title__icontains=keywords)
	context = {
		'listings' : queryset_list,
		
	}
	return render(request,'pages/searched.html',context)
